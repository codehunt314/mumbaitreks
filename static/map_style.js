var entry = "<div class='entry'>\
                        <div class='from'>\
                            <input type='text' name='fromplacename' class='fromplacename'>\
                            <input type='text' name='fromlat' class='fromlat' value='1' readonly>\
                            <input type='text' name='fromlng' class='fromlng' value='2' readonly>\
                        </div>\
                        <div class='to'>\
                            <input type='text' name='toplacename' class='toplacename'>\
                            <input type='text' name='tolat' class='tolat' value='1' readonly>\
                            <input type='text' name='tolng' class='tolng' value='2' readonly>\
                        </div>\
                        <div class='mode'>\
                            <select name='mode'>\
                                <option value='bus'>Bus</option>\
                                <option value='train'>Train</option>\
                                <option value='taxi'>Taxi</option>\
                                <option value='trek'>Trek</option>\
                            </select>\
                        </div>\
                        <div class='distance'>\
                            <input type='text' name='distance'>\
                        </div>\
                        <div class='duration'>\
                            <input type='text' name='duration'>\
                        </div>\
                        <img src='/static/remove.png' class='remove_entry'>\
                        <div style='clear:both;'></div>\
                    </div>"


function set_entry_data(s, from, to){
  s.find('.from').find('input[name=fromplacename]').val(from.placename);
  s.find('.from').find('.fromlat').val(from.lat);
  s.find('.from').find('.fromlng').val(from.lng);
  s.find('.to').find('.toplacename').val(to.placename);
  s.find('.to').find('.tolat').val(to.lat);
  s.find('.to').find('.tolng').val(to.lng);
  if ($("#trekinfo2 input[type=submit]").attr('disabled')){
    $("#trekinfo2 input[type=submit]").removeAttr('disabled');
  }
}

var directionsService = new google.maps.DirectionsService();
function get_direction(pt1, pt2){
  
}
var map_style1 = [
          {
            "featureType": "administrative",
            "stylers": [
              { "visibility": "off" }
            ]
          },{
            "featureType": "administrative.locality",
            "stylers": [
              { "visibility": "on" }
            ]
          },{
            "featureType": "landscape",
            "stylers": [
              { "visibility": "off" }
            ]
          },{
            "featureType": "poi",
            "stylers": [
              { "visibility": "off" }
            ]
          },{
            "featureType": "poi.attraction",
            "stylers": [
              { "visibility": "on" }
            ]
          },{
            "featureType": "road",
            "stylers": [
              { "visibility": "off" }
            ]
          },{
            "featureType": "road.highway",
            "stylers": [
              { "visibility": "simplified" }
            ]
          },{
            "featureType": "road.arterial",
            "stylers": [
              { "visibility": "simplified" }
            ]
          },{
            "featureType": "transit",
            "stylers": [
              { "visibility": "off" }
            ]
          },{
            "featureType": "transit.station.rail",
            "stylers": [
              { "visibility": "on" }
            ]
          },{
            "featureType": "water",
            "stylers": [
              { "visibility": "simplified" }
            ]
          }
        ]


var map_style2 = [
          {
            "featureType": "administrative",
            "stylers": [
              { "visibility": "off" },
            ]
          },{
            "featureType": "administrative.locality",
            "stylers": [
              { "visibility": "on" },
              
            ]
          },{
            "featureType": "landscape",
            "stylers": [
              { "visibility": "on" },
              {
                "hue": "#FFFFFF"
              },
              {
                "saturation": -100
              },
              {
                "lightness": -19.529411764705884
              },
              {
                "gamma": 1
              },
              
            ]
          },{
            "featureType": "poi",
            "stylers": [
              { "visibility": "on" },
              {
                "hue": "#FF0300"
              },
              {
                "saturation": -100
              },
              {
                "lightness": -11.529411764705884
              },
              {
                "gamma": 1
              }
            ]
          },{
            "featureType": "poi.attraction",
            "stylers": [
              { "visibility": "off" }
            ]
          },{
            "featureType": "road",
            "stylers": [
              { "visibility": "off" }
            ]
          },{
            "featureType": "road.highway",
            "stylers": [
              { "visibility": "off" },
              {
                "hue": "#FF0300"
              },
              {
                "saturation": -100
              },
              {
                "lightness": 111.14901960784317
              },
              {
                "gamma": 1
              }
            ]
          },{
            "featureType": "road.arterial",
            "stylers": [
              { "visibility": "off" },
              {
                "hue": "#FF0300"
              },
              {
                "saturation": -100
              },
              {
                "lightness": -98.60392156862746
              },
              {
                "gamma": 1
              }
            ]
          },{
            "featureType": "transit",
            "stylers": [
              { "visibility": "off" }
            ]
          },{
            "featureType": "transit.station.rail",
            "stylers": [
              { "visibility": "on" }
            ]
          },{
            "featureType": "water",
            "stylers": [
              { "visibility": "on" },
              {
                "hue": "#FF0300"
              },
              {
                "saturation": 0
              },
              {
                "lightness": 0
              },
              {
                "gamma": 1
              },
              
            ]
          }
        ]
