/*var trek_route_detail = [
        ['bus.png', 'Khandala', 18.76116,73.38378, 'bus.png', '9am', 'Kataldhara', 18.814627,73.404508, 'arrow.png', '10am'],
        ['trek.png', 'Kataldhara', 18.814627,73.404508, 'arrow.png', '10am', 'Shrivardhan Fort', 18.826732,73.399916, 'peak.png', '1pm'],
        ['trek.png', 'Shrivardhan Fort', 18.826732,73.399916, 'peak.png', '2pm', 'Karjat', 18.861296,73.394594, 'arrow.png', '4pm'],
        ['bus.png', 'Karjat', 18.861296,73.394594, 'arrow.png', '4pm','Karjat Rly Stn', 18.92343,73.325137, 'train.png', '5pm'],
    ]
var trek_route_detail = [
        {
            'from': 'Khanndala',
            'from_lat': 18.76116,
            'from_lng': 73.38378,
            'from_type': 'bus',
            'to': 'Kataldhara',
            'to_lat': 18.76116,
            'to_lng': 73.38378,
            'to_type': 'bus',
            'start_time': '9am',
            'end_time': '10am'
            'mode': 'bus.png'
        }
    ]
*/



var canvas_width = 580;
var canvas_height = 330;

function f(trek_route_detail){
    var lat_list = [];
    var lng_list = [];
    for (var i=0; i<trek_route_detail.length; i++){
        lat_list.push(trek_route_detail[i][2]);
        lat_list.push(trek_route_detail[i][7]);
        lng_list.push(trek_route_detail[i][3]);
        lng_list.push(trek_route_detail[i][8]);
    }
    
    var min_lat = Math.min.apply(null, lat_list) - 0.01;
    var max_lat = Math.max.apply(null, lat_list) + 0.01;
    var min_lng = Math.min.apply(null, lng_list) - 0.01;
    var max_lng = Math.max.apply(null, lng_list) + 0.01;
    var lat_variation = 1.1 * (max_lat - min_lat);
    var lng_variation = 1.1 * (max_lng - min_lng);
    
    var point_list = [];
    for (var i=0; i<trek_route_detail.length; i++){
        var temp = []
        temp.push( parseInt(canvas_height - ((trek_route_detail[i][2]-min_lat) * canvas_height/(lat_variation))) );
        temp.push( parseInt((trek_route_detail[i][3]-min_lng) * canvas_width/(lng_variation)) );
        temp.push( parseInt(canvas_height - ((trek_route_detail[i][7]-min_lat) * canvas_height/(lat_variation))) );
        temp.push( parseInt((trek_route_detail[i][8]-min_lng) * canvas_width/(lng_variation)) );
        point_list.push(temp)
    }
    var x_list = []
    var y_list = []
    for (var i=0; i<point_list.length; i++){
        y_list.push(point_list[i][0]);
        x_list.push(point_list[i][1]);
        y_list.push(point_list[i][2]);
        x_list.push(point_list[i][3]);
    }
    var min_x = Math.min.apply(null, x_list);
    var min_y = Math.min.apply(null, y_list);
    var max_x = Math.max.apply(null, x_list);
    var max_y = Math.max.apply(null, y_list);
    for (var i=0; i<trek_route_detail.length; i++){
        trek_route_detail[i].push(_filter_y(min_y, max_y, point_list[i][0]));
        trek_route_detail[i].push(_filter_x(min_x, max_x, point_list[i][1]));
        trek_route_detail[i].push(_filter_y(min_y, max_y, point_list[i][2]));
        trek_route_detail[i].push(_filter_x(min_x, max_x, point_list[i][3]));
    }
    return trek_route_detail;
}

function _filter_x(min_x, max_x, pt){
    var ret;
    if (pt==min_x){
        ret = {'val': pt, 'flag': 'min'}
    }else if(pt==max_x){
        ret = {'val': pt, 'flag': 'max'}
    }else{
        ret = {'val': pt, 'flag': 'mid'}
    }
    return ret;
}

function _filter_y(min_y, max_y, pt){
    var ret;
    if (pt==min_y){
        ret = {'val': pt, 'flag': 'min'}
    }else if(pt==max_y){
        ret = {'val': pt, 'flag': 'max'}
    }else{
        ret = {'val': pt, 'flag': 'mid'}
    }
    return ret;
}
