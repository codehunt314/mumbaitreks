/*var trek_route_detail = [
        ['bus.png', 'Khandala', 18.76116,73.38378, 'bus.png', '9am', 'Kataldhara', 18.814627,73.404508, 'arrow.png', '10am'],
        ['trek.png', 'Kataldhara', 18.814627,73.404508, 'arrow.png', '10am', 'Shrivardhan Fort', 18.826732,73.399916, 'peak.png', '1pm'],
        ['trek.png', 'Shrivardhan Fort', 18.826732,73.399916, 'peak.png', '2pm', 'Karjat', 18.861296,73.394594, 'arrow.png', '4pm'],
        ['bus.png', 'Karjat', 18.861296,73.394594, 'arrow.png', '4pm','Karjat Rly Stn', 18.92343,73.325137, 'train.png', '5pm'],
    ]
var trek_route_detail = [
        {
            'from': 'Khanndala',
            'from_lat': 18.76116,
            'from_lng': 73.38378,
            'from_type': 'bus',
            'to': 'Kataldhara',
            'to_lat': 18.76116,
            'to_lng': 73.38378,
            'to_type': 'bus',
            'start_time': '9am',
            'end_time': '10am'
            'mode': 'bus.png'
        }
    ]
*/



var canvas_width = 580;
var canvas_height = 330;
var dataPoint = [];
var reverse_map = {};

function latlng_to_canvas_projection(trek_route_detail1){
    var lat_list = [];
    var lng_list = [];
    for (var i=0; i<trek_route_detail1.length; i++){
        lat_list.push(trek_route_detail1[i].from_lat)
        lat_list.push(trek_route_detail1[i].to_lat)
        lng_list.push(trek_route_detail1[i].from_lng)
        lng_list.push(trek_route_detail1[i].to_lng)
    }
    var min_lat = Math.min.apply(null, lat_list) - 0.01;
    var max_lat = Math.max.apply(null, lat_list) + 0.01;
    var min_lng = Math.min.apply(null, lng_list) - 0.01;
    var max_lng = Math.max.apply(null, lng_list) + 0.01;
    var lat_variation = 1.1 * (max_lat - min_lat);
    var lng_variation = 1.1 * (max_lng - min_lng);

    for (var i=0; i<trek_route_detail1.length; i++){
        trek_route_detail1[i]['from_y'] =   parseInt(canvas_height - ((trek_route_detail1[i].from_lat-min_lat) * canvas_height/(lat_variation)));
        trek_route_detail1[i]['from_x'] =   parseInt((trek_route_detail1[i].from_lng-min_lng) * canvas_width/(lng_variation));
        key = trek_route_detail1[i]['from_x'] + ":" + trek_route_detail1[i]['from_y'];
        if (reverse_map.hasOwnProperty(key)){
            reverse_map[key].push({'id': i, 'type': 'from'});
        }else{
            reverse_map[key] = [{'id': i, 'type': 'from'}];
        }
        trek_route_detail1[i]['to_y'] = parseInt(canvas_height - ((trek_route_detail1[i].to_lat-min_lat) * canvas_height/(lat_variation)));
        trek_route_detail1[i]['to_x'] = parseInt((trek_route_detail1[i].to_lng-min_lng) * canvas_width/(lng_variation));
        key = trek_route_detail1[i]['to_x'] + ":" + trek_route_detail1[i]['to_y']
        if (reverse_map.hasOwnProperty(key)){
            reverse_map[key].push({'id': i, 'type': 'to'});
        }else{
            reverse_map[key] = [{'id': i, 'type': 'to'}];
        }
    }
}

function get_distinct_point(trek_detail){
    for (var i=0; i<trek_detail.length; i++){
      var pos = (i%2==0) ? 'left' : 'right'
      if (i==0){
          dataPoint.push([trek_detail[i].from_x, trek_detail[i].from_y])
          dataPoint.push([trek_detail[i].to_x, trek_detail[i].to_y])
      }else{
          if (trek_detail[i].to != trek_detail[0].from){
            dataPoint.push([trek_detail[i].to_x, trek_detail[i].to_y])
          }
      }
    }
    return dataPoint;
}


function dist(i1, i2){
    i1 = parseInt(i1);
    i2 = parseInt(i2)
    x1 = dataPoint[i1][0];
    y1 = dataPoint[i1][1];
    x2 = dataPoint[i2][0];
    y2 = dataPoint[i2][1];
    d = Math.sqrt((x2-x1)*(x2-x1) + (y2-y1)*(y2-y1))
    return d;
}


function rescaleCluster(db) { 
        var bigCluster = null;
        len = 0;
        for (var i=0; i<db.cluster.length; i++){
          if (len<db.cluster[i].length){
            bigCluster = db.cluster[i]
          }
        }

        if (bigCluster==null){
            return false;
        }
        xl = [];
        yl = [];
        for (var i=0; i<bigCluster.length; i++){
          xl.push(dataPoint[bigCluster[i]][0]);
          yl.push(dataPoint[bigCluster[i]][1]);
        }
        min_x_of_cluster = Math.min.apply(null, xl);
        max_x_of_cluster = Math.max.apply(null, xl);
        min_y_of_cluster = Math.min.apply(null, yl);
        max_y_of_cluster = Math.max.apply(null, yl);
        
        left_points_x = [];
        right_points_x = [];
        top_points_y = [];
        bottom_points_y = [];
        for (var i=0; i<db.D.length; i++){
          if (($.inArray(db.D[i][0], xl)==-1) && ($.inArray(db.D[i][1], yl)==-1)){
            if (db.D[i][0] < min_x_of_cluster){
              left_points_x.push(db.D[i][0]);
            }else if(db.D[i][0] > max_x_of_cluster){
              right_points_x.push(db.D[i][0]);
            }
            if (db.D[i][1] < min_y_of_cluster){
              top_points_y.push(db.D[i][1]);
            }else if(db.D[i][1] > max_y_of_cluster){
              bottom_points_y.push(db.D[i][1]);
            }
          }
        }
        x_expansion = min_x_of_cluster - Math.max.apply(null, left_points_x) //+ Math.min.apply(null, right_points_x) - max_x_of_cluster
        y_expansion = min_y_of_cluster - Math.max.apply(null, top_points_y) //+ Math.min.apply(null, bottom_points_y) - max_y_of_cluster
        if (x_expansion>y_expansion){
          yScale = (max_y_of_cluster - min_y_of_cluster)/((max_y_of_cluster - min_y_of_cluster) + y_expansion);
          xScale = yScale * (max_x_of_cluster - min_x_of_cluster)/(max_y_of_cluster - min_y_of_cluster);
        }else{
          xScale = (max_x_of_cluster - min_x_of_cluster)/((max_x_of_cluster - min_x_of_cluster) + x_expansion);
          yScale = xScale * (max_y_of_cluster - min_y_of_cluster)/(max_x_of_cluster - min_x_of_cluster);
        }
        
        if (left_points_x.length>0){
          box_x_left = min_x_of_cluster - (min_x_of_cluster - Math.max.apply(null, left_points_x))*xScale;
        }else{
          box_x_left=min_x_of_cluster
        }
        if (right_points_x.length>0){
          box_x_right = max_x_of_cluster + (Math.min.apply(null, right_points_x) - max_x_of_cluster)*xScale;
        }else{
          box_x_right=max_x_of_cluster
        }
        if (top_points_y.length>0){
          box_y_top = min_y_of_cluster - (min_y_of_cluster - Math.max.apply(null, top_points_y))*yScale;
        }else{
          box_y_top=min_y_of_cluster
        }
        if (bottom_points_y.length>0){
          box_y_bottom = max_y_of_cluster + (Math.min.apply(null, bottom_points_y) - min_y_of_cluster)*yScale;
        }else{
          box_y_bottom=max_y_of_cluster
        }
        ratioX = (box_x_right - box_x_left)/(max_x_of_cluster - min_x_of_cluster)
        ratioY = (box_y_bottom - box_y_top)/(max_y_of_cluster - min_y_of_cluster)
        for (var i=0; i<bigCluster.length; i++){
          new_x = (dataPoint[bigCluster[i]][0] - min_x_of_cluster) * ratioX + box_x_left;
          new_y = (dataPoint[bigCluster[i]][1] - min_y_of_cluster) * ratioY + box_y_top;
          key = dataPoint[bigCluster[i]][0] + ":" + dataPoint[bigCluster[i]][1];
          for (var j=0; j<reverse_map[key].length; j++){
            trek_route_detail1[reverse_map[key][j]['id']][reverse_map[key][j]['type'] + '_x'] = new_x;
            trek_route_detail1[reverse_map[key][j]['id']][reverse_map[key][j]['type'] + '_y'] = new_y;
          }
        }
}

function get_cluster(){
    latlng_to_canvas_projection(trek_route_detail1);
    //dataPoint   =   get_distinct_point(trek_route_detail1)
    //var db = new DBSCAN(dataPoint, dist, 110, 2);
    //db.run();
    //rescaleCluster(db);
    drawTrek(context);
}



