from django.db import models


class Trek(models.Model):
    name        =   models.CharField(max_length=255)
    desc        =   models.TextField(blank=True, null=True)
    trekinfo    =   models.TextField(blank=True, null=True)     #info about trek in json format
    region      =   models.TextField(blank=True, null=True)     #region trek belongs to


class Place(models.Model):
    place_name  =   models.CharField(max_length=255)
    place_lat   =   models.CharField(max_length=255)
    place_lng   =   models.CharField(max_length=255)
    place_type  =   models.CharField(max_length=255)
    desc        =   models.TextField(blank=True, null=True)

class TrekDetails(models.Model):
    trek        =   models.ForeignKey(Trek)
    from_place  =   models.ForeignKey(Place, related_name='from_place')
    to_place    =   models.ForeignKey(Place, related_name='to_place')
    mode        =   models.CharField(max_length=255)
    duration    =   models.CharField(max_length=255)
    distance    =   models.CharField(max_length=255)
    desc        =   models.TextField(blank=True, null=True)
    order       =   models.IntegerField()

