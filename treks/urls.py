from django.conf.urls.defaults import patterns, include, url

urlpatterns = patterns('',
    url(r'^create/', 'treks.views.create_trek'),
    url(r'^(?P<trekid>\d+)/$', 'treks.views.trek_home'),
    url(r'^(?P<trekid>\d+)/add_details/$', 'treks.views.add_details'),
    url(r'^(?P<trekid>\d+)/edit/$', 'treks.views.edit_trek'),
    url(r'^(?P<trekid>\d+)/ajax/$', 'treks.views.ajax'),
)