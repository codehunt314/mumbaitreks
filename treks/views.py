from django.http import HttpResponseRedirect, HttpResponse
from django.shortcuts import render_to_response
from django.template import RequestContext
from django.views.decorators.csrf import csrf_exempt
from django.utils import simplejson
from django.contrib import auth
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from django.contrib.auth.decorators import login_required

from treks.models import *

@csrf_exempt
def register(request):
    if request.method == 'POST':
        username    =   request.POST['username']
        email       =   request.POST['email']
        password1   =   request.POST['password1']
        password2   =   request.POST['password2']
        if password1!=password2:
            return render_to_response('register.html', {'msg': 'password doesn\'t match'})

        user    =   User.objects.create_user(username, email, password1)
        user.is_staff = True
        user.save()
        return HttpResponseRedirect("/login/")
    return render_to_response("register.html", {'msg': None})

@csrf_exempt
def login(request):
    if request.method=='POST':
        username = request.POST['username']
        password = request.POST['password']
        user = auth.authenticate(username=username, password=password)
        if user is not None:
            if user.is_active:
                auth.login(request, user)
                return HttpResponseRedirect('/dashboard/')
            else:
                return HttpResponse('Account is not Active')
        else:
            return HttpResponse('Account is invalid')
    else:
        if request.user.id:
            return HttpResponseRedirect('/dashboard/')
        return render_to_response('login.html')

@csrf_exempt
def logout(request):
    auth.logout(request)
    return HttpResponseRedirect('/')

@login_required
def dashboard(request):
    all_treks       =   Trek.objects.all()
    all_treks_dict  =   {}
    for t in all_treks:
        if t.region in all_treks_dict:
            trekinfo    =   simplejson.loads(t.trekinfo)
            t.trekinfo  =   trekinfo
            all_treks_dict[t.region].append(t)
        else:
            trekinfo    =   simplejson.loads(t.trekinfo)
            t.trekinfo  =   trekinfo
            all_treks_dict[t.region]    =   [t]
    return render_to_response('dashboard.html', {'all_treks_dict': all_treks_dict}, context_instance=RequestContext(request))


def home(request):
    if request.user.id:
        return HttpResponseRedirect('/dashboard/')
    all_treks       =   Trek.objects.all()
    all_treks_dict  =   {}
    for t in all_treks:
        if t.region in all_treks_dict:
            trekinfo    =   simplejson.loads(t.trekinfo)
            t.trekinfo  =   trekinfo
            all_treks_dict[t.region].append(t)
        else:
            trekinfo    =   simplejson.loads(t.trekinfo)
            t.trekinfo  =   trekinfo
            all_treks_dict[t.region]    =   [t]
    return render_to_response('home.html', {'all_treks_dict': all_treks_dict}, context_instance=RequestContext(request))


def leh_plan(request):
    return render_to_response('leh_plan.html', {'itinerary': [['transport', 'stay', 'food']]}, context_instance=RequestContext(request))

@login_required
@csrf_exempt
def create_trek(request):
    if request.method=='POST':
        post_dict       =   dict(request.POST.items())
        trekname        =   post_dict.pop('trekname')
        trekdesc        =   post_dict.pop('desc')
        region          =   post_dict.pop('region')
        trek, created   =   Trek.objects.get_or_create(name=trekname, region=region)
        trek_date       =   request.POST.getlist('date1')
        trek_cost       =   request.POST.getlist('cost')
        trek_source     =   request.POST.getlist('source')
        post_dict.pop('date1')
        post_dict.pop('cost')
        post_dict.pop('source')
        trek_schedule           =   [i for i in zip(trek_date, trek_cost, trek_source) if i[0]!='']
        post_dict['schedule']   =   trek_schedule
        trek.trekinfo           =   simplejson.dumps(post_dict)
        trek.desc               =   trekdesc
        trek.save()
        return HttpResponseRedirect('/treks/%s/add_details/'%(trek.id))
    else:
        return render_to_response('create_trek.html', {}, context_instance=RequestContext(request))


@login_required
@csrf_exempt
def edit_trek(request, trekid):
    trek        =   Trek.objects.get(id=int(trekid))
    trekinfo    =   simplejson.loads(trek.trekinfo)
    schedule    =   trekinfo.pop('schedule')
    if request.method=='POST':
        post_dict       =   dict(request.POST.items())
        trek.name       =   post_dict.pop('trekname')
        trek.desc       =   post_dict.pop('desc')
        trek.region     =   post_dict.pop('region')

        trek_date       =   request.POST.getlist('date1')
        trek_cost       =   request.POST.getlist('cost')
        trek_source     =   request.POST.getlist('source')
        post_dict.pop('date1')
        post_dict.pop('cost')
        post_dict.pop('source')
        post_dict['schedule']   =   [i for i in zip(trek_date, trek_cost, trek_source) if i[0]!='']
        trek.trekinfo           =   simplejson.dumps(post_dict)
        trek.save()
        return HttpResponseRedirect('/treks/%s/add_details/'%(trek.id))
    else:
        return render_to_response(
                                'edit_trek.html', 
                                {
                                    'trek': trek, 
                                    'trekinfo': trekinfo,
                                    'schedule': schedule
                                }, 
                                context_instance=RequestContext(request)
                            )


@login_required
@csrf_exempt
def add_details(request, trekid):
    trek    =   Trek.objects.get(id=int(trekid))
    if request.method=='POST':
        post_data_dict = dict(request.POST)
        trekdetails     =   TrekDetails.objects.filter(trek=trek)
        trekdetails.delete()
        for i in range(len(post_data_dict['toplacename'])):
            from_place, created     =   Place.objects.get_or_create(
                                            place_name  =   post_data_dict['fromplacename'][i],
                                            place_lat   =   post_data_dict['fromlat'][i],
                                            place_lng   =   post_data_dict['fromlng'][i]
                                        )
            to_place, created       =   Place.objects.get_or_create(
                                            place_name  =   post_data_dict['toplacename'][i],
                                            place_lat   =   post_data_dict['tolat'][i],
                                            place_lng   =   post_data_dict['tolng'][i]
                                        )
            order           =   i + 1
            trekdetails, created    =   TrekDetails.objects.get_or_create(
                                                trek        =   trek,
                                                from_place  =   from_place,
                                                to_place    =   to_place,
                                                mode        =   post_data_dict['mode'][i],
                                                duration    =   post_data_dict['duration'][i],
                                                distance    =   post_data_dict['distance'][i],
                                                order       =   order
                                        )
        return HttpResponseRedirect('/treks/%s/'%(trek.id))
    else:
        trekdetails     =   TrekDetails.objects.filter(trek=trek)
        trekinfo        =   simplejson.loads(trek.trekinfo)
        schedule        =   trekinfo.pop('schedule')
        return render_to_response(
                                    'add_details.html', 
                                    {
                                        'trek': trek, 
                                        'trekinfo': trekinfo,
                                        'trekdetails': trekdetails,
                                        'modelist': ['bus', 'train', 'trek', 'taxi']
                                    }, 
                                    context_instance=RequestContext(request)
                            )


def trek_home(request, trekid):
    trek        =   Trek.objects.get(id=int(trekid))
    trekinfo    =   simplejson.loads(trek.trekinfo)
    schedule    =   trekinfo.pop('schedule')
    trekdetail  =   TrekDetails.objects.filter(trek=trek).order_by('order')
    trekdetail_list = []
    trekdetail_list1 = []
    for i in trekdetail:
        trekdetail_list.append([
            '/static/%s.png'%(i.mode),
            i.from_place.place_name, i.from_place.place_lat, i.from_place.place_lng, '/static/bus.png', '9am', 
            i.to_place.place_name, i.to_place.place_lat, i.to_place.place_lng, '/static/arrow.png', '10am'
        ])
        trekdetail_list1.append(
            {
                'from': i.from_place.place_name,
                'from_lat': i.from_place.place_lat,
                'from_lng': i.from_place.place_lng,
                'from_type': '/static/bus.png',
                'to': i.to_place.place_name,
                'to_lat': i.to_place.place_lat,
                'to_lng': i.to_place.place_lng,
                'to_type': '/static/arrow.png',
                'start_time': '9am',
                'end_time': '10am',
                'mode': '/static/%s.png'%(i.mode),
            }
        )
    return render_to_response(
                                'trekhome1.html', 
                                {
                                    'trek': trek, 
                                    'trekdetail': trekdetail, 
                                    'trekdetail_list': simplejson.dumps(trekdetail_list),
                                    'trekdetail_list1': simplejson.dumps(trekdetail_list1),
                                    'trekinfo': trekinfo,
                                    'schedule': schedule,
                                    'user': request.user
                                },
                                context_instance=RequestContext(request)
                        )

