from django.conf.urls import patterns, include, url
import settings
# Uncomment the next two lines to enable the admin:
# from django.contrib import admin
# admin.autodiscover()

urlpatterns = patterns('',
    (r'^$', 'treks.views.home'),
    (r'^login/$', 'treks.views.login'),
    (r'^logout/$', 'treks.views.logout'),
    (r'^register/$', 'treks.views.register'),
    (r'^dashboard/$', 'treks.views.dashboard'),
    (r'^leh/$', 'treks.views.leh_plan'),
    (r'^treks/', include('treks.urls')),
    # Examples:
    # url(r'^$', 'mumbai_treks.views.home', name='home'),
    # url(r'^mumbai_treks/', include('mumbai_treks.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    # url(r'^admin/', include(admin.site.urls)),
    (r'^static/(?P<path>.*)$', 'django.views.static.serve', {'document_root': settings.STATIC_ROOT, 'show_indexes':True})
)
